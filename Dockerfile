# To run shell script
FROM centos:7
LABEL maintainer="vaideek"
WORKDIR /usr/app/src
COPY my_first_script.sh ./
RUN chmod +x ./my_first_script.sh
CMD ["./my_first_script.sh"]
